let username;
let password;
let role;

function loginFunction(){
    username = prompt("Input Username:");
    password = prompt("Input Password:");
    role = prompt("Input Role:");

    if (username === null || username === "" || password === null || password === "" || role === null || role === ""){
        alert("Input must not be empty!");
    } else {
        switch(role.toLowerCase()){
            case "admin":
                alert("Welcome back to the class portal, admin!");
                break;
            case "teacher":
                alert("Thank you for logging in, teacher!");
                break;
            case "student":
                alert("Welcome to the class portal, student!");
                break;
            default:
                alert("Role out of range.");
        }
    }
}
loginFunction();

function checkAverage(q1,q2,q3,q4){
    let avg = Math.round((q1 + q2 + q3 + q4) / 4);
    console.log("checkAverage(" + q1 + "," + q2 + "," + q3 + "," + q4 + ")");
    if (avg <= 74){
        console.log("Hello, student, your average is: " + avg + ". The letter equivalent is F");
    } else if (avg >= 75 && avg <= 79){
        console.log("Hello, student, your average is: " + avg + ". The letter equivalent is D");
    } else if (avg >= 80 && avg <= 84){
        console.log("Hello, student, your average is: " + avg + ". The letter equivalent is C");
    } else if (avg >= 85 && avg <= 89){
        console.log("Hello, student, your average is: " + avg + ". The letter equivalent is B");
    } else if (avg >= 90 && avg <= 95){
        console.log("Hello, student, your average is: " + avg + ". The letter equivalent is A");
    } else {
        console.log("Hello, student, your average is: " + avg + ". The letter equivalent is A+");
    }
    return avg;
}
checkAverage(71,70,73,74);
checkAverage(75,75,76,78);
checkAverage(80,81,82,78);
checkAverage(84,85,87,88);
checkAverage(89,90,91,90);
checkAverage(91,96,97,95);
checkAverage(91,96,97,99);